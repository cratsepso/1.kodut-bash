#!/bin/bash

# Autor: Chris Rätsepso, 2016

# Skriptimiskeeled 1.kodutöö (bash)

#Käesoleva skripti põhieesmärgiks on jagada ettantud grupile uus kaust ning seadistada vastavale grupile jagatud võrguketas kasutades samba failiteenust. 

#Exit koodid: 1-pole juurkasutajana sisse loginud; 2-vale arv etteantud argumente; 3-samba paigaldamisel ilmnes viga; 4-selline jagatud kaust on juba olemas, 5-viga samba konfifailis; 6-kausta loomine ei õnnestunud; 7-kaust on juba olemas


#Kontrollitakse, kas tegemist on juurkasutajaga.

if [ $UID -ne 0 ]
then
    echo "Skript käivitub vaid juurkasutaja (Root) õigustes! Palun logi juurkasutajana sisse!"
    exit 1
fi

# Kontrollitakse, kas kasutaja sisestas õige arvu argumente.

if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <JAGATUD KAUST>"
    exit 2
fi

#Salvestatakse kasutaja poolt sisestatud argumendid muutujatesse.

KAUST=$1
KAUSTBASE=$(basename $KAUST)
GRUPP=$2
JAGKAUST=${3-$KAUSTBASE}




#Kontrollin samba olemasolu ja vajadusel paigaldan selle.

type smbd > /dev/null 2>&1 
 
if [ $? -ne 0 ];
then
  apt-get update && apt-get install samba -y || exit 3
fi

#Kontrollitakse kasutaja poolt sisestatud kausta olemasolu ning vajadusel luuakse see.

if [ -d $KAUST ];
then
  echo "Selline kaust on juba olemas!"
  exit 7
else 
    mkdir -p $KAUST

#Kontrollitakse, kas kausta loomine õnnestus või mitte
    if [ $? -ne 0 ];
    then
        echo "Kausta loomine ei õnnestunud!"
        exit 6
    fi
fi

#Kontrollitakse kasutaja poolt sisestatud grupi olemasolu ning vajadusel luuakse vastav grupp.

getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null



#Kasutaja poolt sisestatud kausta grupiks määratakse kasutaja poolt sisestatud grupp.

chgrp $GRUPP $KAUST
 
 #Samba konfifailist tehakse koopia
 
 cp -n /etc/samba/smb.conf /etc/samba/smb.conf.old 
 
 #Kontrollitakse, kas selline jagatud kaust on juba samba konfifailis olemas ning kui ei ole, siis lisatakse grupi read sinna.
 
 grep "\[$JAGKAUST\]" /etc/samba/smb.conf.old
 
 #Kontrollitakse, kas tegu on täispika teega või mitte
    if [ ${KAUST:0:1} != "/" ];
    then
        KAUST=$(pwd)/$KAUST
    fi
 
 #Täidetakse samba konfifail vastavate andmetega
 cat >> /etc/samba/smb.conf.old << LOPP
 [$JAGKAUST]
  comment=$JAGKAUST kaust
  path=$KAUST
  writable=yes
  valid users=@$GRUPP
  force group=$GRUPP
  browsable=yes
  create mask=0664
  directory=0775

LOPP


 #Testitakse kõigepealt samba konfifaili koopiat. Kui test on edukas, siis viiakse muudatused samba konfifaili sisse ning tehakse samba teenusele reload.
 testparm -s /etc/samba/smb.conf.old > /dev/null
     case "$?" in 
         1)
           echo "Samba konfifailis esines viga!";
           exit 5;
     ;;
         0)
           cp /etc/samba/smb.conf.old /etc/samba/smb.conf && service smbd reload > /dev/null;
     ;;
     esac
